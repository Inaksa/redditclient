//
//  RedditItem.swift
//  redditclient
//
//  Created by Alex Maggio on 20/03/17.
//  Copyright © 2017 Alex Maggio. All rights reserved.
//

import Foundation
import SwiftyJSON

class RedditItem: BaseModel
{
    private let TITLE_KEY       : String = "title"
    private let AUTHOR_KEY      : String = "author"
    private let DATE_KEY        : String = "created"
    private let THUMBNAIL_KEY   : String = "thumbnail"
    private let SUBREDDIT_KEY   : String = "subreddit"
    private let COMMENTS_KEY    : String = "comments"
    private let URL_KEY         : String = "url"
    private let REDDIT_ID_KEY   : String = "name"
    
    // Properties
    // ------------------------------------------
    var redditId    : String    = ""
    var title       : String    = ""
    var author      : String    = ""
    var date        : Date?     = nil
    var subreddit   : String    = ""
    var comments    : Int       = 0
    var url         : String    = ""
    var thumbnail   : String?   = nil
    
    
    // Initializers and Helper functions
    // ------------------------------------------
    init(json:JSON)
    {
        super.init()
        
        load(fromJSON: json)
    }
    
    private func getDateFromString(_ aRawDate : String) -> Date?
    {
        var retValue : Date?
        
        if let epoch = Double(aRawDate)
        {
            retValue = Date(timeIntervalSince1970: epoch)
        }
        
        return retValue
    }
    
    override func load(fromJSON json: JSON)
    {
        super.load(fromJSON: json)
        
        let data = json["data"]
        
        redditId = data[REDDIT_ID_KEY].stringValue
        title = data[TITLE_KEY].stringValue
        author = data[AUTHOR_KEY].stringValue
        url = data[URL_KEY].stringValue
        date = getDateFromString(data[DATE_KEY].stringValue)
        
        thumbnail = data[THUMBNAIL_KEY].string
        
        subreddit = data[SUBREDDIT_KEY].stringValue
        comments = data[COMMENTS_KEY].intValue
    }
    
    var hasThumbnail : Bool
    {
        get
        {
            if let theThumb = thumbnail
            {
                return theThumb.hasPrefix("http")
            }
            else
            {
                return false
            }
            
        }
    }
}
