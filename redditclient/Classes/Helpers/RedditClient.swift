//
//  RedditClient.swift
//  redditclient
//
//  Created by Alex Maggio on 20/03/17.
//  Copyright © 2017 Alex Maggio. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol RedditClientDelegate : NSObjectProtocol
{
    func redditAPISuccessResponse(theItems : [RedditItem])
    func redditAPIFailureResponse(error: Error)
}

class RedditClient: NSObject {
    
    // ALEXM: Notification name when new items arrive
    static let NOTIFY_REFRESH   : String = "newRedditsWereFetched"
    
    static let instance = RedditClient()
    
    var reddits:[RedditItem] = []
    
    var delegate : RedditClientDelegate? = nil
    
    var lastSeen    : String = ""
    var firstSeen   : String = ""
    
    func getItems(numberOfItems: Int)
    {
        print("RedditClient - getRedditData()...")
        
        lastSeen = ""
        firstSeen = ""
        
        reddits.removeAll()
        
        // Construct GET request.
        let requestURL = URL(string:"http://www.reddit.com/top/.json?limit=\(numberOfItems)")!
        performRequest(url: requestURL)
    }
    
    func getTopNextPage(numberOfItems: Int)
    {
        if (numberOfItems < 1) || (lastSeen == "")
        {
            if let del = self.delegate
            {
                del.redditAPISuccessResponse(theItems: [])
            }
        }
        let requestURL = URL(string:"http://www.reddit.com/top/.json?limit=\(numberOfItems)&after=\(lastSeen)")!
        self.reddits.removeAll()
        performRequest(url: requestURL)
    }

    func getTopPreviousPage(numberOfItems: Int)
    {
        if (numberOfItems < 1) || (firstSeen == "")
        {
            if let del = self.delegate
            {
                del.redditAPISuccessResponse(theItems: [])
            }
        }
        let requestURL = URL(string:"http://www.reddit.com/top/.json?limit=\(numberOfItems)&before=\(firstSeen)")!
        self.reddits.removeAll()
        performRequest(url: requestURL)
    }
    
    func performRequest(url theURL: URL)
    {
        let request = NSMutableURLRequest(url: theURL)
        request.httpMethod = "GET"
        
        // Async GET
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            print("RedditClient - Data retrieved.")
            
            if error != nil
            {
                print("error retrieving data")
                if let del = self.delegate
                {
                    del.redditAPIFailureResponse(error: error!)
                }
                return
            }
            
            let json = JSON(data: data!)
            //            print("json: \(json)")
            
            let reddits = json["data"]["children"]      // REDDIT API Describes the response as having this structure.
            for (_, entry) in reddits
            {
                let reddit = RedditItem(json: entry)
                self.reddits.append(reddit)
                self.lastSeen = reddit.redditId
                self.firstSeen = self.reddits[0].redditId
            }
            
            if let del = self.delegate
            {
                del.redditAPISuccessResponse(theItems: self.reddits)
            }
        })
        
        task.resume()
    }
}
