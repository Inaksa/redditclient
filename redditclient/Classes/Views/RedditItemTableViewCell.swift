//
//  RedditItemTableViewCell.swift
//  redditclient
//
//  Created by Alex Maggio on 20/03/17.
//  Copyright © 2017 Alex Maggio. All rights reserved.
//

import Foundation
import UIKit

class RedditItemTableViewCell : UITableViewCell
{
    @IBOutlet var subreddit : UILabel!
    @IBOutlet var author    : UILabel!
    @IBOutlet var title     : UILabel!
    @IBOutlet var thumbnail : UIImageView!
    
    private var currentPreview : String = ""
    private let formatter : DateFormatter = DateFormatter()
    
    
    
    func configure(withRedditItem theItem: RedditItem)
    {
        subreddit.text = theItem.subreddit
        title.text = theItem.title
        
        if let postDate = theItem.date
        {
            formatter.dateFormat = "hh:mm MM.dd.yyyy"
            author.text = String(format: "%@ - %@", theItem.author, formatter.string(from: postDate))
        }
        if let theThumbnail = theItem.thumbnail
        {
            downloadImage(theThumbnail)
        }
        else
        {
            thumbnail.isHidden = true
        }
        
    }
    
    func downloadImage(_ thumb : String)
    {
        currentPreview = thumb
        
        if !thumb.hasPrefix("http")
        {
            thumbnail.isHidden = true
            return
        }
        thumbnail.isHidden = true
        let theURL : URL = URL(string: thumb)!
        
        getDataFromUrl(url: theURL)
        { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let theImage = UIImage(data: data)
                else
            {
                return
            }
            
            if self.currentPreview == response?.url?.absoluteString
            {
                DispatchQueue.main.async() { () -> Void in
                    print("Image Arrived for: \(self.currentPreview)")
                    self.thumbnail.isHidden = false
                    self.thumbnail.image = theImage
//                    self.thumbnail.setNeedsDisplay()
                    
                }
            }
        }
//        thumbnail.downloadedFrom(url: theURL)
//        thumbnail.af_setImage(withURL: theURL)
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void)
    {
        URLSession.shared.dataTask(with: url)
        {
            (data, response, error) in
            completion(data, response, error)
        }.resume()
    }
}
