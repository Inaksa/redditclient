//
//  RedditItemTableViewCellNoPicture.swift
//  redditclient
//
//  Created by Alex Maggio on 20/03/17.
//  Copyright © 2017 Alex Maggio. All rights reserved.
//

import Foundation
import UIKit

class RedditItemTableViewCellNoPicture : RedditItemTableViewCell
{
    private let formatter : DateFormatter = DateFormatter()
    
    override func configure(withRedditItem theItem: RedditItem)
    {
        subreddit.text = theItem.subreddit
        title.text = theItem.title
        
        if let postDate = theItem.date
        {
            formatter.dateFormat = "hh:mm MM.dd.yyyy"
            author.text = String(format: "%@ - %@", theItem.author, formatter.string(from: postDate))
        }
    }
}
