//
//  ViewController.swift
//  redditclient
//
//  Created by Alex Maggio on 20/03/17.
//  Copyright © 2017 Alex Maggio. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class ViewController: UIViewController {
    
    @IBOutlet var redditItemTable : UITableView!

    @IBOutlet var nextPage          : UIBarButtonItem!
    @IBOutlet var prevPage          : UIBarButtonItem!
    
    fileprivate let resultsPerPage  : Int = 25
    fileprivate var currentPage     : Int = 0
    
    
    func setUserInteraction(_ aValue: Bool)
    {
        self.redditItemTable.isUserInteractionEnabled = aValue
        
        guard let leftButtons = self.navigationItem.leftBarButtonItems,
              let rightButtons = self.navigationItem.rightBarButtonItems
        else
        {
            return
        }
        
        let theButtons = leftButtons + rightButtons
        
        for aButton in theButtons
        {
            aButton.isEnabled = aValue
        }
        
        if currentPage == 0
        {
            prevPage.isEnabled = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
        RedditClient.instance.delegate = self
        handleRefreshReddits(nil)
    }
    
    func requestsReddits()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        setUserInteraction(false)
        RedditClient.instance.getItems(numberOfItems: resultsPerPage)
    }

    @IBAction func handleRefreshReddits(_ sender: AnyObject?)
    {
        currentPage = 0
        requestsReddits()
    }
    
    @IBAction func advancePage(_ sender: AnyObject?)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        setUserInteraction(false)
        RedditClient.instance.getTopNextPage(numberOfItems: resultsPerPage)
        currentPage += 1
        
    }
    
    @IBAction func rewindPage(_ sender: AnyObject?)
    {
        if currentPage < 1
        {
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        setUserInteraction(false)
        RedditClient.instance.getTopPreviousPage(numberOfItems: resultsPerPage)
        currentPage -= 1
    }
    
    
}

extension ViewController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RedditClient.instance.reddits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let theItem = RedditClient.instance.reddits[indexPath.row]
    
        var retValue : RedditItemTableViewCell
        if theItem.hasThumbnail
        {
            retValue = tableView.dequeueReusableCell(withIdentifier: "RedditItemCell", for: indexPath) as! RedditItemTableViewCell
        }
        else
        {
            retValue = tableView.dequeueReusableCell(withIdentifier: "RedditItemCellNoImage", for: indexPath) as! RedditItemTableViewCellNoPicture
        }
        
        retValue.configure(withRedditItem: theItem)
        return retValue
    }
}

extension ViewController : RedditClientDelegate
{
    func redditAPIFailureResponse(error: Error) {
        MBProgressHUD.hide(for: self.view, animated: true)
        setUserInteraction(true)
        let theAlert : UIAlertController = UIAlertController(title: "Reddit Client", message: "There was an error while trying to get the top \(resultsPerPage) posts of Reddit", preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Try Again", style: .default)
        { (action) in
            RedditClient.instance.getItems(numberOfItems: self.resultsPerPage)
        }
        theAlert.addAction(retryAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        theAlert.addAction(cancelAction)
        
        present(theAlert, animated: true, completion: nil)
    }
    
    func redditAPISuccessResponse(theItems: [RedditItem]) {
        OperationQueue.main.addOperation
        {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.setUserInteraction(true)
            
            if self.currentPage == 0
            {
                self.title = "Reddit Top 25"
            }
            else
            {
                self.title = "Top 25 - Page \(self.currentPage + 1)"
            }
            self.redditItemTable.reloadData()
        }
        
    }
}
